# Battle Training

This gRPC server and client can be used for randomly generating spaceship info with the parameters described below, as well as validate said ships using pydantic and store the data inside a pSQL database.


## Parameters

Each ship had several characteristics:

- Alignment (ally/enemy) - enum;
- Name (can be 'Unknown' for enemy ships) - string;
- Class, which is one of {Corvette, Frigate, Cruiser, Destroyer, Carrier, Dreadnought} - enum;
- Length in meters - float;
- Size of the crew - integer;
- Whether the ship is armed or not - bool;
- List of officers in charge of the ship. Each officer on board should have first name, last name, and rank as strings.


### Ship validation

Each ship class has a number of parameters that are validated

| Class       | Length     | Crew    | Can be armed? | Can be hostile? |
|-------------|------------|---------|---------------|-----------------|
| Corvette    | 80-250     | 4-10    | Yes           | Yes             |
| Frigate     | 300-600    | 10-15   | Yes           | No              |
| Cruiser     | 500-1000   | 15-30   | Yes           | Yes             |
| Destroyer   | 800-2000   | 50-80   | Yes           | No              |
| Carrier     | 1000-4000  | 120-250 | No            | Yes             |
| Dreadnought | 5000-20000 | 300-500 | Yes           | Yes             |


## Workflow 

To generate pb2 files, you can call the `make gRPC` target.

The workflow is as follows:

1) the server is started;
2) the client is started, given a set of coordinates in some chosen form, e.g:
    
`~$ ./reporting_client_v1.py 17 45 40.0409 −29 00 28.118`

3) these coordinates are sent to the server, and the server responds with a random (1-10) number of ships in a gRPC stream to the client;
4) the client prints all received ships to standard output as a set of serialized JSON strings.

For shorthand you call one of the three clients:
- `make client_v1` for simple ship generation (print out all ships)
- `make client_v2` for ship generation with validation (print only valid ships)
- `make client_v3_scan` for adding valid generated ships to the database (print only valid ships)
- `make client_v3_traitors` for finding traitors in the database (prints list of traitors only)
