from sqlalchemy import create_engine, Column, ForeignKey, UniqueConstraint, func
from sqlalchemy import Integer, String, Boolean, Float
from sqlalchemy.orm import sessionmaker, declarative_base
import json

Base = declarative_base()

ALIGNMENTS = ['ALLY', 'ENEMY']
SHIP_CLASSES = ['CORVETTE', 'FRIGATE', 'CRUISER', 'DESTROYER', 'CARRIER', 'DREADNOUGHT']

class Alignment(Base):
    __tablename__ = 'alignments'

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)

    __table_args__ = (
        UniqueConstraint('name', name='unique_alignment'),  
    )


class ShipClass(Base):
    __tablename__ = 'ship_classes'

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)

    __table_args__ = (
        UniqueConstraint('name', name='unique_ship_class'),  
    )


class Officer(Base):
    __tablename__ = 'officers'

    id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)
    rank = Column(String)
    __table_args__ = (
        UniqueConstraint('first_name', 'last_name', 'rank', name='unique_officer'),
    )


class OfficerOnShip(Base):
    __tablename__ = 'officers_on_ship'

    id = Column(Integer, primary_key=True)
    officer_id = Column(Integer, ForeignKey('officers.id'))
    ship_id = Column(Integer, ForeignKey('spaceships.id'))
    __table_args__ = (
        UniqueConstraint('officer_id', 'ship_id', name='unique_officers_on_ship'),
    )


class Spaceship(Base):
    __tablename__ = 'spaceships'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    ship_class = Column(Integer, ForeignKey('ship_classes.id'))
    length = Column(Float)
    crew_size = Column(Integer)
    armed = Column(Boolean)
    alignment = Column(Integer, ForeignKey('alignments.id'))


class pSQL_Agent:
    def __init__(self, restart=False):
        self.engine = create_engine('postgresql://postgres:password@localhost:5432/s21_p06_ships')
        Base.metadata.create_all(self.engine)
        self.Session = sessionmaker(bind=self.engine)
        self.session = self.Session()
        if restart:
            self.initialize_database()

    def initialize_database(self):
        Base.metadata.drop_all(self.engine, checkfirst=True)
        Base.metadata.create_all(self.engine)

        alignments = []
        for alignment in ALIGNMENTS:
            alignments.append(Alignment(name=alignment))

        ship_classes = []
        for ship_class in SHIP_CLASSES:
            ship_classes.append(ShipClass(name=ship_class))

        self.session.add_all(alignments)
        self.session.add_all(ship_classes)
        self.session.commit()

    def list_traitors(self):
        subquery = self.session.query(
            OfficerOnShip.officer_id, func.count(func.distinct(Spaceship.alignment)).label('num_alignments')
        ).join(Spaceship, OfficerOnShip.ship_id == Spaceship.id).group_by(OfficerOnShip.officer_id).having(func.count(func.distinct(Spaceship.alignment)) > 1).subquery()

        query = self.session.query(Officer.first_name, Officer.last_name, Officer.rank).join(subquery, Officer.id == subquery.c.officer_id)

        results = query.all()
        results_dict = [{'first_name': row.first_name, 'last_name': row.last_name, 'rank': row.rank} for row in results]
        for traitor in results_dict:
            print(json.dumps(traitor, indent=2))

    def add_ships(self, message):
        for ship in message:
            new_spaceship = Spaceship(name=ship.name,
                                      ship_class=ship.ship_class+1, # babe, this is hardcoding and you know it
                                      length=ship.length, 
                                      crew_size=ship.crew_size, 
                                      armed=ship.armed, 
                                      alignment=ship.alignment+1)
            self.session.add(new_spaceship)
            self.session.commit()
            for officer in ship.officers:
                duplicate_officers = self.session.query(Officer).filter(
                    Officer.first_name == officer.first_name,
                    Officer.last_name == officer.last_name,
                    Officer.rank == officer.rank
                ).all()

                if len(duplicate_officers) == 0:
                    new_officer = Officer(first_name=officer.first_name,
                                      last_name=officer.last_name,
                                      rank=officer.rank)
                    self.session.add(new_officer)
                    self.session.commit()
                    id = new_officer.id
                else: 
                    id = duplicate_officers[0].id

                new_officer_on_ship = OfficerOnShip(officer_id=id, ship_id=new_spaceship.id)
                self.session.add(new_officer_on_ship)
                self.session.commit()


if __name__ == '__main__':
    new_api = pSQL_Agent(restart=True)
