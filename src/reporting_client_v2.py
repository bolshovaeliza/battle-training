import grpc
import argparse
import ship_pb2
import ship_pb2_grpc
import logging
from google.protobuf.json_format import MessageToJson
from ship_generator import Ship



class Client:
    def __init__(self, host='localhost', port=8080):
        self.channel = grpc.insecure_channel(f'{host}:{port}')
        self.stub = ship_pb2_grpc.SpaceshipStreamStub(self.channel)
        logging.basicConfig(level=logging.INFO)
        self.logger = logging.getLogger(__name__)

    def get_response(self, *coords):
        response = {
            'latitude_h': int(coords[0]),
            'latitude_m': int(coords[1]),
            'latitude_s': float(coords[2]),
            'longitude_h': int(coords[3]),
            'longitude_m': int(coords[4]),
            'longitude_s': float(coords[5])
        }
        message = self.stub.GetServerResponse(ship_pb2.Coordinates(**response))
        message_list = list()
        error_counter = 0
        for msg in message:
            msg_json = MessageToJson(msg, preserving_proto_field_name=True)

            try:
                Ship.parse_raw(msg_json)
                print("\n ~~~~~~~~~~~~~~~~~ SHIP INFORMATION ~~~~~~~~~~~~~~~~~ ")
                print(msg_json)
                message_list.append(msg)
            except ValueError as e:
                error_counter += 1

        print(f"\n\nINVALID SHIPS: {error_counter} out of {len(message_list)}")
        return message_list


def main():
    parser = argparse.ArgumentParser(description='grpc client')
    parser.add_argument("latitude_h", type=int, help="latitude_h")
    parser.add_argument("latitude_m", type=int, help="latitude_m")
    parser.add_argument("latitude_s", type=float, help="latitude_s")
    parser.add_argument("longitude_h", type=int, help="longitude_h")
    parser.add_argument("longitude_m", type=int, help="longitude_m")
    parser.add_argument("longitude_s", type=float, help="longitude_s")
    args = parser.parse_args()
    client = Client()
    client.get_response(args.latitude_h, args.latitude_m, args.latitude_s, args.longitude_h, args.longitude_m,
                        args.longitude_s)


if __name__ == '__main__':
    main()
