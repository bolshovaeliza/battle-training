import argparse
from psql_agent import pSQL_Agent
from reporting_client_v2 import Client


def main():
    parser = argparse.ArgumentParser(description='grpc client that allows adding values to an sql database and scanning for traitors')
    subparser = parser.add_subparsers(dest='command')

    parser_scan = subparser.add_parser('scan', help='Scan ships in the area')
    parser_scan.add_argument("latitude_h", type=int, help="latitude_h")
    parser_scan.add_argument("latitude_m", type=int, help="latitude_m")
    parser_scan.add_argument("latitude_s", type=float, help="latitude_s")
    parser_scan.add_argument("longitude_h", type=int, help="longitude_h")
    parser_scan.add_argument("longitude_m", type=int, help="longitude_m")
    parser_scan.add_argument("longitude_s", type=float, help="longitude_s")
    
    parser_list = subparser.add_parser('list_traitors', help='List all traitors on the ships')
    
    args = parser.parse_args()
    sql_agent = pSQL_Agent()

    if args.command == 'scan':
        client = Client()
        message = client.get_response(args.latitude_h, args.latitude_m, args.latitude_s, args.longitude_h, args.longitude_m,
                            args.longitude_s)
        sql_agent.add_ships(message)
        

    elif args.command == 'list_traitors':
        sql_agent.list_traitors()


if __name__ == '__main__':
    main()
