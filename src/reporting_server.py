import grpc
import ship_pb2_grpc
from concurrent import futures
import random
import ship_generator as sg
import ship_pb2
import argparse


class Service(ship_pb2_grpc.SpaceshipStreamServicer):
    def __init__(self, ship_generator_func):
        self.ship_generator_func = ship_generator_func

    def GetServerResponse(self, request, context):
        ships = [ship_pb2.Spaceship(**self.ship_generator_func()) for _ in range(random.randint(1, 10))]
        yield from ships


def serve(ship_generator_func):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=5))
    ship_pb2_grpc.add_SpaceshipStreamServicer_to_server(Service(ship_generator_func), server)
    server.add_insecure_port('[::]:8080')
    server.start()
    print('Server Started')
    server.wait_for_termination()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Server for spaceship generation")
    parser.add_argument("--valid", "-v", action='store_true', help="Only create valid spaceships")
    args = parser.parse_args()
    if (args.valid):
        serve(sg.valid_ship)
    else:
        serve(sg.random_ship)


