DROP TABLE IF EXISTS spaceships CASCADE;
DROP TABLE IF EXISTS officers CASCADE;
DROP TABLE IF EXISTS officers_on_ship CASCADE;

SELECT * FROM spaceships;
SELECT * FROM officers;
SELECT * FROM officers_on_ship;
SELECT * FROM alignments;
SELECT * FROM ship_classes;

/* List of spaceships */
SELECT spaceships.id,
       spaceships.name as ship_name, 
       ship_classes.name as class, 
       alignments.name as alignment,
       length,
       crew_size,
       armed
FROM spaceships
JOIN ship_classes ON spaceships.ship_class = ship_classes.id
JOIN alignments ON spaceships.alignment = alignments.id;

/* List of officers on each ship */
SELECT spaceships.name as ship_name,
       alignments.name as alignment,
       officers.first_name, 
       officers.last_name, 
       officers.rank
FROM officers_on_ship
JOIN officers ON officers_on_ship.officer_id = officers.id
JOIN spaceships ON officers_on_ship.ship_id = spaceships.id
JOIN alignments ON spaceships.alignment = alignments.id;

/* List of traitors */
SELECT o.first_name, o.last_name, o.rank
FROM officers o
JOIN (
    SELECT os.officer_id, COUNT(DISTINCT s.alignment) as num_alignments
    FROM officers_on_ship os
    JOIN spaceships s ON os.ship_id = s.id
    GROUP BY os.officer_id
    HAVING COUNT(DISTINCT s.alignment) > 1
) AS multiple_alignments ON o.id = multiple_alignments.officer_id;

