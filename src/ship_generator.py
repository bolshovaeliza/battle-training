import random
import ship_pb2
from enum import Enum
from pydantic import BaseModel, validator
from typing import Optional

officers_names = ('John', 'Mary', 'David', 'Karen', 'Steven', 'Alan',
                  'Liam', 'Noah', 'Oliver', 'James', 'Elijah',
                  'Mateo', 'Theodore', 'Henry', 'Lucas', 'William',
                  'Olivia', 'Emma', 'Charlotte', 'Amelia', 'Isabella',
                  'Mia', 'Evelyn', 'Luna')


officers_surnames = ('Smith', 'Johnson', 'Williams', 'Jones', 'Brown', 'Shepard',
                     'Miller', 'Davis', 'Rodriguez', 'Martinez', 'Hernandez', 'Lopez',
                     'Gonzales', 'Wilson', 'Anderson', 'Thomas', 'Taylor', 'Moore',
                     'Jackson', 'Martin', 'Lee', 'Perez', 'Thompson', 'White', 'Harris',
                     'Sanchez', 'Clark', 'Ramirez', 'Lewis')
officers_ranks = ('Captain', 'Lieutenant', 'Sergeant', 'Commander')

enemies_spaceships = ('Death Star', 'Executor', 'Star Destroyer', 'Imperial Shuttle', 'Unknown')
ally_spaceships = ('Normandy', 'X-Wing', 'Millennium Falcon')

traitors = (
    {'first_name': 'John', 'last_name': 'Doe', 'rank': 'Professional problem'},
    {'first_name': 'Jane', 'last_name': 'Doe', 'rank': 'Certified cheat'},
    {'first_name': 'Jim', 'last_name': 'Doe', 'rank': 'Sanctioned swindler'},
    {'first_name': 'Jack', 'last_name': 'Doe', 'rank': 'Proficient pretender'}
)


def random_ship():
    ship = ship_pb2.Spaceship()
    ship.ship_class = random.choice(list(ShipClass))
    ship.alignment = random.choice(list(Alignment))
    ship.name = get_ship_name(ship.alignment)
    if ship.alignment == 1:
        officers_num = random.randint(0, 10)
    else:
        officers_num = random.randint(1, 10)
    ship.length = round(random.uniform(80.0, 20000.0), 1)
    ship.crew_size = random.randint(4, 500)
    ship.armed = random.randint(0, 1)
    result_value = {
        'alignment': ship.alignment,
        'name': ship.name,
        'ship_class': ship.ship_class,
        'length': ship.length,
        'crew_size': ship.crew_size,
        'armed': ship.armed,
        'officers': get_officer_list(officers_num)
    }
    return result_value


def valid_ship():
    ship = ship_pb2.Spaceship()
    ship_class = random.choice(list(ShipClass))
    ship.ship_class = ship_class
    if ship_class == ShipClass.DESTROYER or ship_class == ShipClass.FRIGATE:
        ship.alignment = Alignment.ALLY
    else:
        ship.alignment = random.choice(list(Alignment))
    ship.name = get_ship_name(ship.alignment)
    if ship.alignment == 1:
        officers_num = random.randint(0, 10)
    else:
        officers_num = random.randint(1, 10)
    length = ShipClass.get_ship_length(ship_class)
    ship.length = round(random.uniform(length[0], length[1]), 1)
    crew_size = ShipClass.get_ship_crew_size(ship_class)
    ship.crew_size = random.randint(crew_size[0], crew_size[1])
    if ship_class == ShipClass.CARRIER:
        ship.armed = 0
    else:
        ship.armed = random.randint(0, 1)
    result_value = {
        'alignment': ship.alignment,
        'name': ship.name,
        'ship_class': ship.ship_class,
        'length': ship.length,
        'crew_size': ship.crew_size,
        'armed': ship.armed,
        'officers': get_officer_list(officers_num)
    }
    return result_value


def get_ship_name(alignment):
    if alignment == 1:
        return random.choice(enemies_spaceships) + '-' + str(random.randint(1000, 9999))
    else:
        return random.choice(ally_spaceships) + '-' + str(random.randint(1000, 9999))
    

def get_officer_list(officer_number):
    officers = []
    for _ in range(officer_number):
        new_officer = get_officer()
        while (new_officer in officers):
            new_officer = get_officer()
        officers.append(new_officer)
    return officers


def get_officer():
    return random.choice(({
        "first_name": random.choice(officers_names),
        "last_name": random.choice(officers_surnames),
        "rank": random.choice(officers_ranks)
    }, random.choice(traitors)))


class Alignment(str, Enum):
    ALLY = 'ALLY'
    ENEMY = 'ENEMY'


class ShipClass(str, Enum):
    CORVETTE = 'CORVETTE'
    FRIGATE = 'FRIGATE'
    CRUISER = 'CRUISER'    
    DESTROYER = 'DESTROYER'
    CARRIER = 'CARRIER'
    DREADNOUGHT = 'DREADNOUGHT'

    def get_ship_length(ship_class):
        if ship_class == ShipClass.CORVETTE:
            return (80, 250)
        elif ship_class == ShipClass.FRIGATE:
            return (300, 600)
        elif ship_class == ShipClass.CRUISER:
            return (500, 1000)
        elif ship_class == ShipClass.DESTROYER:
            return (800, 2000)
        elif ship_class == ShipClass.CARRIER:
            return (1000, 4000)
        elif ship_class == ShipClass.DREADNOUGHT:
            return (2000, 5000)
        else:
            raise ValueError('Invalid ship class')
        
    def get_ship_crew_size(ship_class):
        if ship_class == ShipClass.CORVETTE:
            return (4, 10)
        elif ship_class == ShipClass.FRIGATE:
            return (10, 15)
        elif ship_class == ShipClass.CRUISER:
            return (15, 30)
        elif ship_class == ShipClass.DESTROYER:
            return (50, 80)
        elif ship_class == ShipClass.CARRIER:
            return (120, 250)
        elif ship_class == ShipClass.DREADNOUGHT:
            return (300, 500)
        else:
            raise ValueError('Invalid ship class')


class Officer(BaseModel):
    first_name: str
    last_name: str
    rank: str


class Ship(BaseModel):
    name: str
    ship_class: ShipClass
    length: float
    crew_size: int
    armed: bool
    alignment: Alignment
    officers: Optional[list[Officer]] = None

    @validator('name')
    def validate_name(cls, name, values):
        if name == 'Unknown' and values.get('alignment') == Alignment.ALLY:
            raise ValueError(f"The Ship with name {name} cannot be ally")
        return name

    @validator('armed')
    def validate_armed(cls, armed, values):
        if values.get('ship_class') == ShipClass.CARRIER and armed:
            raise ValueError(f"{values.get('ship_class')} cannot be armed")
        return armed

    @validator('alignment')
    def validate_alignment(cls, alignment, values):
        if values.get('ship_class') in [ShipClass.FRIGATE, ShipClass.DESTROYER] and alignment == Alignment.ENEMY:
            raise ValueError(f"{values.get('ship_class')} cannot be hostile")
        return alignment

    @validator('ship_class')
    def validate_ship_class(cls, ship_class, values):
        if ship_class not in ShipClass:
            print("\n\nvalid: I got this class", ship_class)
            raise ValueError('Invalid ship class')
        return ship_class

    @validator('length')
    def validate_length(cls, length, values):
        ship_class = values.get('ship_class')
        min_length, max_length = ShipClass.get_ship_length(ship_class)
        if not (min_length <= length <= max_length):
            raise ValueError(f"Invalid length for {ship_class} (expected {min_length}-{max_length})")
        return values

    @validator('crew_size')
    def validate_crew_size(cls, crew_size, values):
        ship_class = values.get('ship_class')
        min_crew_size, max_crew_size = ShipClass.get_ship_crew_size(ship_class)
        if not (min_crew_size <= crew_size <= max_crew_size):
            raise ValueError(f"Invalid crew size for {ship_class} (expected {min_crew_size}-{max_crew_size})")
        return values

    @validator('officers', pre=True, always=True)
    def validate_officers(cls, officers, values):
        if values.get('alignment') == Alignment.ALLY:
            if not officers or len(officers) < 1:
                raise ValueError("Invalid number of officers for an ALLY ship (expected at least 1)")
        return officers
