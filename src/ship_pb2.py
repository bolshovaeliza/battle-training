# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ship.proto
# Protobuf Python Version: 5.26.1
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\nship.proto\"\x80\x03\n\tSpaceship\x12\'\n\talignment\x18\x01 \x02(\x0e\x32\x14.Spaceship.Alignment\x12\x0c\n\x04name\x18\x02 \x02(\t\x12$\n\nship_class\x18\x03 \x02(\x0e\x32\x10.Spaceship.Class\x12\x0e\n\x06length\x18\x04 \x02(\x02\x12\x11\n\tcrew_size\x18\x05 \x02(\x05\x12\r\n\x05\x61rmed\x18\x06 \x02(\x08\x12$\n\x08officers\x18\x07 \x03(\x0b\x32\x12.Spaceship.Officer\x1a>\n\x07Officer\x12\x12\n\nfirst_name\x18\x01 \x02(\t\x12\x11\n\tlast_name\x18\x02 \x02(\t\x12\x0c\n\x04rank\x18\x03 \x02(\t\" \n\tAlignment\x12\x08\n\x04\x41LLY\x10\x00\x12\t\n\x05\x45NEMY\x10\x01\"\\\n\x05\x43lass\x12\x0c\n\x08\x43ORVETTE\x10\x00\x12\x0b\n\x07\x46RIGATE\x10\x01\x12\x0b\n\x07\x43RUISER\x10\x02\x12\r\n\tDESTROYER\x10\x03\x12\x0b\n\x07\x43\x41RRIER\x10\x04\x12\x0f\n\x0b\x44READNOUGHT\x10\x05\"\x88\x01\n\x0b\x43oordinates\x12\x12\n\nlatitude_h\x18\x01 \x02(\x05\x12\x12\n\nlatitude_m\x18\x02 \x02(\x05\x12\x12\n\nlatitude_s\x18\x03 \x02(\x02\x12\x13\n\x0blongitude_h\x18\x04 \x02(\x05\x12\x13\n\x0blongitude_m\x18\x05 \x02(\x05\x12\x13\n\x0blongitude_s\x18\x06 \x02(\x02\x32\x44\n\x0fSpaceshipStream\x12\x31\n\x11GetServerResponse\x12\x0c.Coordinates\x1a\n.Spaceship\"\x00\x30\x01')

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'ship_pb2', _globals)
if not _descriptor._USE_C_DESCRIPTORS:
  DESCRIPTOR._loaded_options = None
  _globals['_SPACESHIP']._serialized_start=15
  _globals['_SPACESHIP']._serialized_end=399
  _globals['_SPACESHIP_OFFICER']._serialized_start=209
  _globals['_SPACESHIP_OFFICER']._serialized_end=271
  _globals['_SPACESHIP_ALIGNMENT']._serialized_start=273
  _globals['_SPACESHIP_ALIGNMENT']._serialized_end=305
  _globals['_SPACESHIP_CLASS']._serialized_start=307
  _globals['_SPACESHIP_CLASS']._serialized_end=399
  _globals['_COORDINATES']._serialized_start=402
  _globals['_COORDINATES']._serialized_end=538
  _globals['_SPACESHIPSTREAM']._serialized_start=540
  _globals['_SPACESHIPSTREAM']._serialized_end=608
# @@protoc_insertion_point(module_scope)
